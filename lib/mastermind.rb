class Code
  attr_reader :pegs

  def initialize(code)
    @pegs = code
  end

  PEGS = {
    "B" => :Blue,
    "G" => :Green,
    "O" => :Orange,
    "P" => :Purple,
    "Y" => :Yellow,
    "R" => :Red
  }
  def self.parse(input)
    input.upcase!
    input.each_char do |ch|
      unless PEGS.keys.include?(ch)
        raise "Incorrect Input"
      end
    end
    Code.new(input.chars)
  end

  def self.random
    res = []
    4.times {res << PEGS.keys.sample}
    Code.new(res)
  end

  def [](idx)
    pegs[idx]
  end

  def exact_matches(new_code)
    count = 0
    new_code.pegs.each_with_index do |peg, i|
      count += 1 if self.pegs[i] == peg
    end
    count
  end

  def near_matches(new_code)
    count = 0
    dupped_self = self.pegs.dup
    new_code.pegs.each do |peg|
      if dupped_self.include?(peg)
        count += 1
        dupped_self.delete_at(dupped_self.index(peg))
      end
    end
    count - self.exact_matches(new_code)
  end

  def ==(new_code)
    return false if new_code.class != Code
    self.pegs == new_code.pegs
  end
end

class Game
  attr_reader :secret_code

  def initialize(code = Code.random)
    @secret_code = code
  end

  def get_guess
    puts "Type in a code bbbb"
    Code.parse(gets.chomp)
  end

  def display_matches(code)
    puts "near matches: #{secret_code.near_matches(code)}"
    puts "exact matches: #{secret_code.exact_matches(code)}"
  end

end
